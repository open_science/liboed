// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//  Created by Mikhail Kalugin on 07 / 09 / 2020.
//  Copyright 2020 Mikhail Kalugin All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissionsand
// limitations under the License.
//
// (C) 2020 ������ �������
//
// ������������� �������� �������� Apache, ������ 2.0 ("��������");
// �� ������ ������������ ���� ���� ������ � ������������ � ���������.
// �� ������ ����� ����� �������� �� ������
// http://www.apache.org/licenses/LICENSE-2.0
//
// �� ����������� �������, ����� ��� ���������������� ������������
// ����������������� ��� ���� ��� �� ��������� � ���������� ����������,
// ����������� ����������� ���������������� �� �������� ������ ��������,
// ��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
// ���������� �� �������� ������ � ������������, ����������� � �������������
// ����� �������� ��������, �� ������ ����� � ������ ��������.

#include <catch2/catch.hpp>
#include "../src/liboed.h"
#include <string>

SCENARIO("Verify version present", "[test_Version.cpp]")
{
    REQUIRE(oed::version_str() == "0.0.1");
}

SCENARIO("Verify standard version present", "[test_Version.cpp]")
{
    REQUIRE(oed::std_version_str() == "0.0.1");
}

