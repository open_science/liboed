#pragma once
//  Created by Mikhail Kalugin on 07 / 09 / 2020.
//  Copyright 2020 Mikhail Kalugin All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissionsand
// limitations under the License.
//
// (C) 2020 Михаил Калугин
//
// Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");
// вы можете использовать этот файл только в соответствии с Лицензией.
// Вы можете найти копию Лицензии по адресу
// http://www.apache.org/licenses/LICENSE-2.0
//
// За исключением случаев, когда это регламентировано существующим
// законодательством или если это не оговорено в письменном соглашении,
// программное обеспечение распространяемое на условиях данной Лицензии,
// предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.
// Информацию об основных правах и ограничениях, применяемых к определенному
// языку согласно Лицензии, вы можете найти в данной Лицензии.

/** \file Workspace.h \brief Рабочее пространство OED
*
*/

namespace oed
{

    /** \brief Класс, реализующий функционал рабочего простраства
    *
    */
    class Workspase
    {
    };

}