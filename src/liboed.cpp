// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//  Created by Mikhail Kalugin on 07 / 09 / 2020.
//  Copyright 2020 Mikhail Kalugin All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissionsand
// limitations under the License.
//
// (C) 2020 Михаил Калугин
//
// Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");
// вы можете использовать этот файл только в соответствии с Лицензией.
// Вы можете найти копию Лицензии по адресу
// http://www.apache.org/licenses/LICENSE-2.0
//
// За исключением случаев, когда это регламентировано существующим
// законодательством или если это не оговорено в письменном соглашении,
// программное обеспечение распространяемое на условиях данной Лицензии,
// предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.
// Информацию об основных правах и ограничениях, применяемых к определенному
// языку согласно Лицензии, вы можете найти в данной Лицензии.


#include "config.h"
#include "liboed.h"

namespace oed
{
    const std::string version = "0.0.1";
    const std::string std_version = "0.0.1";

    const std::string& version_str()
    {
        return version;
    }

    const std::string& std_version_str()
    {
        return std_version;
    }
}
