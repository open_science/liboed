# Требования к контейнеру
1. Возможность открытия встроенными/широко распространенными средствами
2. Открытость спецификации
3. Наличие реализаций для основных языков программирования (в том числе
   в том числе MatLab, Modelica и т.д.)

# Выбор типа контейнера
Для обеспечения вышеуказанных требований наиболее подходящим считаем
файлы архива zip.
