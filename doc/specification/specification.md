# Формат файла OED, версия 0.01.0
## Термины и определения
1. Контейнер - файл содержащий в себе наборы данных. Файловая система, внутри 
файла.
2. Тяжелые данные - наборы данных полученые в результате измерений или 
расчетов.

## Общие сведения
Файл OED (open engeniring data, открытые инженерные данные) представляет собой
контейнер Zip https://pkware.cachefly.net/webdocs/casestudies/APPNOTE.TXT

Контейнер OED должен содержать в корневом каталоге файл manifest.rdf,
соедержащий описание содержимого контейнра включая временные отметки и
хэши (sha3) тяжелых данных.

Секция расширенной информации Zip с идентификатором 2 файла manifest.rdf
должна содержать хэш данного файла в формате sha3. Расхождение сохраненного
и вычисленного при чтение хэша не допускается.

## Безопасность
### Контроль целостности
Стандартн zip предусматривает использование контрольных сумм (CRC) в качестве
механизма контроля целостности. В качестве дополнительной меры предусмотрена
возможность хранения хэша. Для тяжелых данных хеширование является обязательным.
Использование хеша в данном случае служит двум целям:
1. Позволяет гарантировать целостность тяжелых данных не зависимо от того,
   предусматривает формат контроль целостности или нет.
2. Позволяет однозначно идентифицировать набор данных не зависимо от принятой
   реализацией схемы именования (например с целью исключения дублирования).

Для прочих данных использование хэша является опциональным.

### Шифрование
В случае необходимости контейнер может быть зашифрован. Для шифрования должен
применяться алгоритм WinZip (как описано в https://www.winzip.com/aes_info.htm).
Для шифрования используются: 
1. Алгоритм генерации ключа - PBKDF2 (http://www.faqs.org/rfcs/rfc2898.html)
2. Алгоритм шифрования - AES с длинной ключа 128, 192 или 256


## Структура manifest.rdf
Файл manifest.rdf представляет собой последовательность триплентов rdf в 
представление RDF/XML. Рекомендуется использовать кодировку UTF-8. Во всех
случаях, когда используется кодировка UNICODE отичная от UTF-8 использование
маркера порядка байтов (BOM) является обязательным.

Каждый узел первого уровня содержит описание одного 
файла внутри контейнера (кроме manifest.rdf).

Для описания базовых свойств файла используется словарь Dublin Core. 
Свойства type и format должны быть определены для каждого узла. Первый 
определяет назначение файла, второй определяет тип данных файла в нотации MIME. 
Элементы Creator, Date и Description являются не обязательными. Элемент Creator 
содержит имя создателя файла, элемент Date содержит дату и время создания
файла в формате ДД.ММ.ГГГГ:ЧЧ:MM:CC.мммм где ДД-день, ММ-месяц, ГГГГ-год,
ЧЧ-часы (в 24-часовом формате), ММ-минуты мммм-микросекунды (опционально)
создания файла. Все элементы даты записываются с ведущими нулями.
Элемент Description содержит описание файла. Остальные элемнт Dublin Core 
не используются.

Для описания свойств файла так же используется словарь Open Science Core.
Элемент timestamp содержит временную метку, преставляющую собой микросекунды 
от начала самого раннего файла данных или мультимедиа в контейнере. Каждая
временная метка кодируется элементом rdf:Seq из двух элементов, обозначающих
начало и конец записи. Опциональный элемент Signals содержит перечень сигналов, 
содержащихся в наборе данных.

## Словарь Open Science Core
Словарь описывает основные элементы, используемые для описания мета-данных
Open Science.

### Свойство timestamp
Временная отметка, обычно представляет собой последовательность из двух
элементов. Обозначает начало и конец события.

### Свойство hash
Хэш, сохраненный в формате <алгоритм>:<октеты>, где: алгоритм - каноническое
обозначение алгоритма (в текущей реализации sha3); октеты - текстовое 
представление октетов хеша в виде шестнадцатиричных чисел.

### Элемент Signals
Содержит список имен всех сигналов в наборе данных.

## Использование унифицированных идентификаторов ресурса (URI)
Унифицированные идентификаторы ресурса (URL) используются для того, чтобы
указать на файл, описываемый данным узлом. Используются схемы file: (RFC8089)
и arcp: (https://datatracker.ietf.org/doc/draft-soilandreyes-arcp/). В обоих
случаях используется UNIX-подобная организация файловой системы где за корень
принимается уровень на единицу выше уровня файла manifest.rdf. Выход за границы
контейнера не допускается, кроме случая указания размещения при использование
arcp, при этом указание размещения меняет расположение корневого узла 
виртуальной файловой системы, но выход за граници c помщью обратных ссылок (..) 
так же не допускается.

Использование схем, отличных от перечисленных выше не допускается. В целях
обеспечения обратной совместимости все узлы описаний с схемами, отличными от 
перечисленны должны игнорироваться. Использование неизвестной схемы не должно
считаться ошибкой.

Во всех случаях, когда схема URL это допускает, рекомендуется применять прямое
хранение октетов (кодировка UTF-8) без использования процентной нотации кроме
случая когда кодировка manifest.rdf отличается от UTF-8 (в этом случае 
применение процентной нотации обязательно).

## Типы данных (значения свойства Dublin Core type)
### Data
Содержит тяжелые данные (записи инструментов, дампы, результаты рассчетов и 
т.д).

### Comment
Комментарий. Обычно представляет собой текстовый блок, содержащий описание
эксперимента или испытания, либо другие сведения, необходимые для интерпретации
данных.

### Media
Растровое или вектороное изображение, аудио или видео запись. Данные 
записывающих устройств, регистрирующих ход испытания (кроме записей 
инструментов, не предполагающих неспоредственный просмотр человеком - АЦП 
измерительного оборудования, средства журналирования цифровых шин и т.д.)

## Синхронизация
С целью обеспечения синхронизации элементов контейнера все элеметы типов Data
и Media должны содержать свойство "timestamp". Данное свойтсво содержит смещение
в микросекундах относительно самой ранней записи.

Для самой ранней записи значение начала всегда равно нулю. Значение конца равно
продолжительности записи в микросекундах.

## Каталоги
